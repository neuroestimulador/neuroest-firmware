//-------------------------------------------Protocolo Basal unipolar:
//Timer 1 (12 (inv), 11 (n-inv), 13 (n-inv))
//Timer 3 (5 (inv), 2 (n-inv), 3 (n-inv))
//Timer 4 (7 (inv), 6 (n-inv), 8 (n-inv))
//Timer 5 (45 (inv), 46 (n-inv), 44 (n-inv))
//Limites testados:
//T entre 20us e 1000us
//IPI entre 1ms e 64ms
//ITI entre 1s e 60s

//Bibliotecas
#include <avr/io.h>						    //inclui biblioteca de input/output do AVR
#include <avr/eeprom.h>					  //inclui biblioteca de EEPROM
#include <avr/interrupt.h>				//inclui biblioteca de interrupt
#include <stdlib.h>							  //inclui biblioteca padrao
#include <string.h>							  //inclui biblioteca de strings

//Variaveis globais
unsigned int H = 1000;                   //define largura de pulso (em us)
unsigned char IPI = 1;                   //define intervalo entre pulsos (em ms) 
unsigned int pulso=0;
unsigned char estado=1;
unsigned int ITI = 1;                    	//define intervalo entre trens de pulso (em s) 

//Rotinas de interrupcao
ISR(TIMER1_COMPA_vect)										//interrupt quando acontece match entre OCR1X e TCNT1 (PWM baixa)
{
	pulso++;																 //incrementa contagem de pulso
	if(pulso==2 && estado == 1)							 //concluídos 2 pulsos, desativa PWM
	{    
    TCCR1A &= ~(1 << COM1A0);    
    TCCR1A &= ~(1 << COM1A1);
    TCCR1A &= ~(1 << COM1B0);
    TCCR1A &= ~(1 << COM1B1);
    TCCR1A &= ~(1 << COM1C0);
    TCCR1A &= ~(1 << COM1C1);    
    TCCR3A &= ~(1 << COM3A0);    
    TCCR3A &= ~(1 << COM3A1);
    TCCR3A &= ~(1 << COM3B0);
    TCCR3A &= ~(1 << COM3B1);
    TCCR3A &= ~(1 << COM3C0);
    TCCR3A &= ~(1 << COM3C1); 
    TCCR4A &= ~(1 << COM4A0);      
    TCCR4A &= ~(1 << COM4A1);
    TCCR4A &= ~(1 << COM4B0);
    TCCR4A &= ~(1 << COM4B1);
    TCCR4A &= ~(1 << COM4C0);
    TCCR4A &= ~(1 << COM4C1);
    TCCR5A &= ~(1 << COM5A0);
    TCCR5A &= ~(1 << COM5A1);
    TCCR5A &= ~(1 << COM5B0);
    TCCR5A &= ~(1 << COM5B1);
    TCCR5A &= ~(1 << COM5C0);
    TCCR5A &= ~(1 << COM5C1);
   	estado = 0;											    //entra em estado de PWM desligado
		pulso=0;                            //zera contagem de pulsos
	}
	if(pulso==((1000000*ITI+1000*IPI)/(H+1000*IPI)) && estado == 0)       //concluído intervalo de tempo desligado ITI, reativa PWM
	{
    TCCR1A |= ((1 << COM1A1) | (1 << COM1B1) | (1 << COM1B0) | (1 << COM1C1));
    TCCR1A &= ~(1 << COM1A0);
    TCCR1A &= ~(1 << COM1C0);    
    TCCR3A |= ((1 << COM3A1) | (1 << COM3B1) | (1 << COM3B0) | (1 << COM3C1));
    TCCR3A &= ~(1 << COM3A0);
    TCCR3A &= ~(1 << COM3C0);  
    TCCR4A |= ((1 << COM4A1) | (1 << COM4B1) | (1 << COM4B0) | (1 << COM4C1));  
    TCCR4A &= ~(1 << COM4A0);  
    TCCR4A &= ~(1 << COM4C0); 
    TCCR5A |= ((1 << COM5A1) | (1 << COM5B1) | (1 << COM5B0) | (1 << COM5C1));  
    TCCR5A &= ~(1 << COM5A0);  
    TCCR5A &= ~(1 << COM5C0); 
		pulso = 0;													//limpa quantidade de pulsos
		estado = 1;										      //entra em estado de PWM ligado
	}
}

//Funcao principal
int main()														                      //funcao principal
{
  config_timer1();                                      //realiza configuracao do timer 1 - canal 1
  config_timer3();                                      //realiza configuracao do timer 3 - canal 2
  config_timer4();                                      //realiza configuracao do timer 4 - canal 3
  config_timer5();                                      //realiza configuracao do timer 5 - canal 4           
  while(1); 
	return 0;
}
void config_timer1()													//funcao que configura o timer 1 do microcontrolador
{
	cli();														          //desativa interrupts para fazer configuracao
	//PWM
  //Ativa pinos do canal 1:
	DDRB |= (1 << PB5);                //ativa pino PB5 (OC1A) - pino 11
	DDRB |= (1 << PB6);                //ativa pino PB6 (OC1B) - pino 12
  DDRB |= (1 << PB7);                //ativa pino PB7 (OC1C) - pino 13
  //Limpa registradores de controle do timer 1
  TCCR1A = 0;
  TCCR1B = 0;
  //Define timer 1 no modo Fast-PWM 14 (TOP definido por ICR1)
  TCCR1A |= (1 << WGM11);
  TCCR1B |= (1 << WGM12);
  TCCR1B |= (1 << WGM13);
  //Saida de PWM nao-inversor em A e inversor em B e nao-inversor em C
  TCCR1A |= ((1 << COM1A1) | (1 << COM1B1) | (1 << COM1B0) | (1 << COM1C1));
  TCCR1A &= ~(1 << COM1A0);
  TCCR1A &= ~(1 << COM1C0);
  // Define frequencia do PWM e Duty Cycle
  ICR1 = ((IPI*1000 + H)/4-1);
	OCR1A = ICR1/(((1000*IPI)+H)/H);
	OCR1B = ICR1/(((1000*IPI)+H)/H);
  OCR1C = ICR1/(((1000*IPI)+H)/H);
  //Inicia timer 1
	TCCR1B |= ((1 << CS11)| (1 << CS10));        //define prescaler de 64 para timer 1 e incia
	//Ativa interrupt do timer 1:
	TIMSK1 |= (1<<OCIE1A);
	sei();																			 //Reativa os interrupts
}

void config_timer3()                          //funcao que configura o timer 3 do microcontrolador
{
  cli();                             //desativa interrupts para fazer configuracao
  //PWM
  //Ativa pinos do canal 3:
  DDRE |= (1 << PE3);                //ativa pino PE3 (OC3A) - pino 2
  DDRE |= (1 << PE4);                //ativa pino PE4 (OC3B) - pino 3
  DDRE |= (1 << PE5);                //ativa pino PE5 (OC3C) - pino 5
  //Limpa registradores de controle do timer 3
  TCCR3A = 0;
  TCCR3B = 0;
  //Define timer 3 no modo Fast-PWM 14 (TOP definido por ICR3)
  TCCR3A |= (1 << WGM31);
  TCCR3B |= (1 << WGM32);
  TCCR3B |= (1 << WGM33);
  //Saida de PWM nao-inversor em A e inversor em B e nao-inversor em C
  TCCR3A |= ((1 << COM3A1) | (1 << COM3B1) | (1 << COM3B0) | (1 << COM3C1));
  TCCR3A &= ~(1 << COM3A0);
  TCCR3A &= ~(1 << COM3C0);
  // Define frequencia do PWM e Duty Cycle
  ICR3 = ((IPI*1000 + H)/4-1);
  OCR3A = ICR3/(((1000*IPI)+H)/H);
  OCR3B = ICR3/(((1000*IPI)+H)/H);
  OCR3C = ICR3/(((1000*IPI)+H)/H);
  //Inicia timer 3
  TCCR3B |= ((1 << CS31)| (1 << CS30));        //define prescaler de 64 para timer 3 e inicia  
  sei();                                       //Reativa os interrupts
}
void config_timer4()                          //funcao que configura o timer 4 do microcontrolador
{
  cli();                             //desativa interrupts para fazer configuracao
  //PWM
  //Ativa pinos do canal 3:
  DDRH |= (1 << PH3);                //ativa pino PH3 (OC4A) - pino 6
  DDRH |= (1 << PH4);                //ativa pino PH4 (OC4B) - pino 7
  DDRH |= (1 << PH5);                //ativa pino PH5 (OC4C) - pino 8
  //Limpa registradores de controle do timer 4
  TCCR4A = 0;
  TCCR4B = 0;
  //Define timer 4 no modo Fast-PWM 14 (TOP definido por ICR4)
  TCCR4A |= (1 << WGM41);
  TCCR4B |= (1 << WGM42);
  TCCR4B |= (1 << WGM43);
  //Saida de PWM nao-inversor em A e inversor em B e nao-inversor em C
  TCCR4A |= ((1 << COM4A1) | (1 << COM4B1) | (1 << COM4B0) | (1 << COM4C1));
  TCCR4A &= ~(1 << COM4A0);
  TCCR4A &= ~(1 << COM4C0);
  // Define frequencia do PWM e Duty Cycle
  ICR4 = ((IPI*1000 + H)/4-1);
  OCR4A = ICR4/(((1000*IPI)+H)/H);
  OCR4B = ICR4/(((1000*IPI)+H)/H);
  OCR4C = ICR4/(((1000*IPI)+H)/H);
  //Inicia timer 4
  TCCR4B |= ((1 << CS41)| (1 << CS40));        //define prescaler de 64 para timer 4 e inicia  
  sei();                                       //Reativa os interrupts
}

void config_timer5()                          //funcao que configura o microcontrolador
{
  cli();                                      //desativa interrupts para fazer configuracao
  //PWM
  //Ativa pinos do canal 4:
  DDRL |= (1 << PL3);                //ativa pino PL3 (OC5A) - pino 46
  DDRL |= (1 << PL4);                //ativa pino PL4 (OC5B) - pino 45
  DDRL |= (1 << PL5);                //ativa pino PL5 (OC5C) - pino 44
  //Limpa registradores de controle do timer 5
  TCCR5A = 0;
  TCCR5B = 0;
  //Define timer 5 no modo Fast-PWM 14 (TOP definido por ICR5)
  TCCR5A |= (1 << WGM51);
  TCCR5B |= (1 << WGM52);
  TCCR5B |= (1 << WGM53);
  //Saida de PWM nao-inversor em A e inversor em B e nao-inversor em C
  TCCR5A |= ((1 << COM5A1) | (1 << COM5B1) | (1 << COM5B0) | (1 << COM5C1));
  TCCR5A &= ~(1 << COM5A0);
  TCCR5A &= ~(1 << COM5C0);
  // Define frequencia do PWM e Duty Cycle
  ICR5 = ((IPI*1000 + H)/4-1);
  OCR5A = ICR5/(((1000*IPI)+H)/H);
  OCR5B = ICR5/(((1000*IPI)+H)/H);
  OCR5C = ICR5/(((1000*IPI)+H)/H);
  //Inicia timer 5
  TCCR5B |= ((1 << CS51)| (1 << CS50));        //define prescaler de 64 para timer 5 e inicia  
  sei();                                       //Reativa os interrupts
}
