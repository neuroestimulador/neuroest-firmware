## Firmware para neuroestimulador

Esse é um firmware para conexão serial com o computador (diálogo com software de interface gráfica) e geração dos protocolos de estímulo necessários para pesquisas científicas. Ele tem capacidade de gerar protocolos Basal, TBS (Theta Burst Stimulation), PBS (Primed Burst Stimulation) e HFS (High Frequency Stimulation) com parâmetros variáveis de acordo com o que é selecionado pelo usuário via software. 
Ele é compatível com Arduino MEGA 2560 R3 e utiliza diferentes saídas de PWM para geração dos protocolos. Boa parte da programação se dá em nível de registradores do ATmega2560 em linguagem C.



## Especificações gerais

* 4 canais de saída de protocolo (permite aplicação de 4 sinais bipolares em tecidos neuronais)
* Cada canal contém 3 saídas: A (não-inversora), B (inversora) e C(não-inversora)
* Saídas A e B para geração de sinal bipolar e saída C para registro de estímulo
* Tensão dos pulsos de saída: ~5V
* Corrente máxima das saídas: 40mA por saída ou 300mA no total


## Licença
A presente documentação descreve hardware, firmware e software de código aberto.
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licença Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />O trabalho <span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Neuroestimulador</span> de <a xmlns:cc="http://creativecommons.org/ns#" href="http://cta.if.ufrgs.br" property="cc:attributionName" rel="cc:attributionURL">Luís Eduardo Estradioto</a> está licenciado com uma Licença <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons - Atribuição-CompartilhaIgual 4.0 Internacional</a>.