//Bibliotecas
#include <avr/io.h>                //inclui biblioteca de input/output do AVR
#include <avr/eeprom.h>           //inclui biblioteca de EEPROM
#include <avr/interrupt.h>        //inclui biblioteca de interrupt
#include <stdlib.h>               //inclui biblioteca padrao
#include <string.h>               //inclui biblioteca de strings
#include <math.h>


//Variaveis globais
unsigned char protocol = 0;
unsigned int H = 200;                   //define largura de pulso (em us)
unsigned char IPI = 1;                   //define intervalo entre pulsos (em ms) 
unsigned long int pulso=0;
unsigned char estado=1;
unsigned int ITI = 10;                      //define intervalo entre trens de pulso (em s) 


unsigned int IEI = 200;                  //define intervalo entre epocas (em ms)

unsigned char n_pulso = 4;
unsigned char n_epoca = 10;
unsigned int epoca = 0;
unsigned int contador = 0;

//Rotinas de interrupcao
ISR(TIMER1_COMPA_vect)                    //interrupt quando acontece match entre OCR1X e TCNT1 (PWM baixa)
{
  switch(protocol)  
  {
    case 'b':
    //PROTOCOLO BASAL UNIPOLAR
    pulso++;                                 //incrementa contagem de pulso
    if(pulso==2 && estado == 1)              //concluídos 2 pulsos, desativa PWM
    {    
      TCCR1A &= ~(1 << COM1A0);    
      TCCR1A &= ~(1 << COM1A1);
      TCCR1A &= ~(1 << COM1B0);
      TCCR1A &= ~(1 << COM1B1);
      TCCR1A &= ~(1 << COM1C0);
      TCCR1A &= ~(1 << COM1C1);    
      TCCR3A &= ~(1 << COM3A0);    
      TCCR3A &= ~(1 << COM3A1);
      TCCR3A &= ~(1 << COM3B0);
      TCCR3A &= ~(1 << COM3B1);
      TCCR3A &= ~(1 << COM3C0);
      TCCR3A &= ~(1 << COM3C1); 
      TCCR4A &= ~(1 << COM4A0);      
      TCCR4A &= ~(1 << COM4A1);
      TCCR4A &= ~(1 << COM4B0);
      TCCR4A &= ~(1 << COM4B1);
      TCCR4A &= ~(1 << COM4C0);
      TCCR4A &= ~(1 << COM4C1);
      TCCR5A &= ~(1 << COM5A0);
      TCCR5A &= ~(1 << COM5A1);
      TCCR5A &= ~(1 << COM5B0);
      TCCR5A &= ~(1 << COM5B1);
      TCCR5A &= ~(1 << COM5C0);
      TCCR5A &= ~(1 << COM5C1);
      estado = 0;                         //entra em estado de PWM desligado
      pulso=0;                            //zera contagem de pulsos
    }
    if(pulso==((1000000*ITI+1000*IPI)/(H+1000*IPI)) && estado == 0)       //concluído intervalo de tempo desligado ITI, reativa PWM
    {
      TCCR1A |= ((1 << COM1A1) | (1 << COM1B1) | (1 << COM1B0) | (1 << COM1C1));
      TCCR1A &= ~(1 << COM1A0);
      TCCR1A &= ~(1 << COM1C0);    
      TCCR3A |= ((1 << COM3A1) | (1 << COM3B1) | (1 << COM3B0) | (1 << COM3C1));
      TCCR3A &= ~(1 << COM3A0);
      TCCR3A &= ~(1 << COM3C0);  
      TCCR4A |= ((1 << COM4A1) | (1 << COM4B1) | (1 << COM4B0) | (1 << COM4C1));  
      TCCR4A &= ~(1 << COM4A0);  
      TCCR4A &= ~(1 << COM4C0); 
      TCCR5A |= ((1 << COM5A1) | (1 << COM5B1) | (1 << COM5B0) | (1 << COM5C1));  
      TCCR5A &= ~(1 << COM5A0);  
      TCCR5A &= ~(1 << COM5C0); 
      pulso = 0;                          //limpa quantidade de pulsos
      estado = 1;                         //entra em estado de PWM ligado
    }
    break;
    case 't':
    //PROTOCOLO TBS UNIPOLAR
    pulso++;                                 //incrementa contagem de pulso
  if(epoca==(n_epoca+1))
  {
    if(contador == ((ITI)/(IPI+H)))
    {
      epoca = 0;      
      estado = 1;      
      pulso = 0;
      contador = 0;      
    }
    else
    {
      contador++;
    }
  }
  else
  {
    if(pulso==n_pulso && estado == 1)              //se tem o terceiro pulso, desativa PWM  
    {
      TCCR1A &= ~(1 << COM1A0);      
      TCCR1A &= ~(1 << COM1A1);  
      TCCR1A &= ~(1 << COM1B0);  
      TCCR1A &= ~(1 << COM1B1);  
      TCCR1A &= ~(1 << COM1C0);  
      TCCR1A &= ~(1 << COM1C1);
      TCCR3A &= ~(1 << COM3A0);      
      TCCR3A &= ~(1 << COM3A1);  
      TCCR3A &= ~(1 << COM3B0);  
      TCCR3A &= ~(1 << COM3B1);  
      TCCR3A &= ~(1 << COM3C0);  
      TCCR3A &= ~(1 << COM3C1);
      TCCR4A &= ~(1 << COM4A0);      
      TCCR4A &= ~(1 << COM4A1);  
      TCCR4A &= ~(1 << COM4B0);  
      TCCR4A &= ~(1 << COM4B1);  
      TCCR4A &= ~(1 << COM4C0);  
      TCCR4A &= ~(1 << COM4C1);
      TCCR5A &= ~(1 << COM5A0);  
      TCCR5A &= ~(1 << COM5A1);  
      TCCR5A &= ~(1 << COM5B0);  
      TCCR5A &= ~(1 << COM5B1);  
      TCCR5A &= ~(1 << COM5C0);  
      TCCR5A &= ~(1 << COM5C1);  
      estado = 0;                         //entra em estado de PWM desligado  
      pulso=0;  
      epoca++;                            //incrementa contagem de epoca  
    }  
    if(pulso==((IPI+IEI)/(IPI+H)) && estado == 0)  
    {      
      TCCR1A |= ((1 << COM1A1) | (1 << COM1B1) | (1 << COM1B0) | (1 << COM1C1));  
      TCCR1A &= ~(1 << COM1A0);  
      TCCR1A &= ~(1 << COM1C0);  
      TCCR3A |= ((1 << COM3A1) | (1 << COM3B1) | (1 << COM3B0) | (1 << COM3C1));  
      TCCR3A &= ~(1 << COM3A0);  
      TCCR3A &= ~(1 << COM3C0); 
      TCCR4A |= ((1 << COM4A1) | (1 << COM4B1) | (1 << COM4B0) | (1 << COM4C1));  
      TCCR4A &= ~(1 << COM4A0);
      TCCR4A &= ~(1 << COM4C0); 
      TCCR5A |= ((1 << COM5A1) | (1 << COM5B1) | (1 << COM5B0) | (1 << COM5C1));  
      TCCR5A &= ~(1 << COM5A0);  
      TCCR5A &= ~(1 << COM5C0);      
      pulso = 0;                          //limpa quantidade de pulsos  
      estado = 1;                         //entra em estado de PWM ligado  
    }
  }
    
  }
  
}

//Funções de configuração de protocolo:
void config_desativado()                                                                      //configura desativação de protocolos
{
  DDRB |= (0 << PB5);                //ativa pino PB5 (OC1A) - pino 11
  DDRB |= (0 << PB6);                //ativa pino PB6 (OC1B) - pino 12
  DDRB |= (0 << PB7);                //ativa pino PB7 (OC1C) - pino 13
  DDRE |= (0 << PE3);                //ativa pino PE3 (OC3A) - pino 2
  DDRE |= (0 << PE4);                //ativa pino PE4 (OC3B) - pino 3
  DDRE |= (0 << PE5);                //ativa pino PE5 (OC3C) - pino 5
  DDRH |= (0 << PH3);                //ativa pino PH3 (OC4A) - pino 6
  DDRH |= (0 << PH4);                //ativa pino PH4 (OC4B) - pino 7
  DDRH |= (0 << PH5);                //ativa pino PH5 (OC4C) - pino 8
  DDRL |= (0 << PL3);                //ativa pino PL3 (OC5A) - pino 46
  DDRL |= (0 << PL4);                //ativa pino PL4 (OC5B) - pino 45
  DDRL |= (0 << PL5);                //ativa pino PL5 (OC5C) - pino 44
}
void config_basal_unipolar()                                                                  //configura protocolo basal unipolar
{
  //--------------------------------------------TIMER 1:
  cli();                                      //desativa interrupts para fazer configuracao
  //PWM
  //Ativa pinos do canal 1:
  DDRB |= (1 << PB5);                //ativa pino PB5 (OC1A) - pino 11
  DDRB |= (1 << PB6);                //ativa pino PB6 (OC1B) - pino 12
  DDRB |= (1 << PB7);                //ativa pino PB7 (OC1C) - pino 13
  //Limpa registradores de controle do timer 1
  TCCR1A = 0;
  TCCR1B = 0;
  //Define timer 1 no modo Fast-PWM 14 (TOP definido por ICR1)
  TCCR1A |= (1 << WGM11);
  TCCR1B |= (1 << WGM12);
  TCCR1B |= (1 << WGM13);
  //Saida de PWM nao-inversor em A e inversor em B e nao-inversor em C
  TCCR1A |= ((1 << COM1A1) | (1 << COM1B1) | (1 << COM1B0) | (1 << COM1C1));
  TCCR1A &= ~(1 << COM1A0);
  TCCR1A &= ~(1 << COM1C0);
  // Define frequencia do PWM e Duty Cycle
  ICR1 = ((IPI*1000 + H)/4-1);
  OCR1A = ICR1/(((1000*IPI)+H)/H);
  OCR1B = ICR1/(((1000*IPI)+H)/H);
  OCR1C = ICR1/(((1000*IPI)+H)/H);
  //Inicia timer 1
  TCCR1B |= ((1 << CS11)| (1 << CS10));        //define prescaler de 64 para timer 1 e incia
  //Ativa interrupt do timer 1:
  TIMSK1 |= (1<<OCIE1A);
  //--------------------------------------------TIMER 3:
  //PWM
  //Ativa pinos do canal 2:
  DDRE |= (1 << PE3);                //ativa pino PE3 (OC3A) - pino 2
  DDRE |= (1 << PE4);                //ativa pino PE4 (OC3B) - pino 3
  DDRE |= (1 << PE5);                //ativa pino PE5 (OC3C) - pino 5
  //Limpa registradores de controle do timer 3
  TCCR3A = 0;
  TCCR3B = 0;
  //Define timer 3 no modo Fast-PWM 14 (TOP definido por ICR3)
  TCCR3A |= (1 << WGM31);
  TCCR3B |= (1 << WGM32);
  TCCR3B |= (1 << WGM33);
  //Saida de PWM nao-inversor em A e inversor em B e nao-inversor em C
  TCCR3A |= ((1 << COM3A1) | (1 << COM3B1) | (1 << COM3B0) | (1 << COM3C1));
  TCCR3A &= ~(1 << COM3A0);
  TCCR3A &= ~(1 << COM3C0);
  // Define frequencia do PWM e Duty Cycle
  ICR3 = ((IPI*1000 + H)/4-1);
  OCR3A = ICR3/(((1000*IPI)+H)/H);
  OCR3B = ICR3/(((1000*IPI)+H)/H);
  OCR3C = ICR3/(((1000*IPI)+H)/H);
  //Inicia timer 3
  TCCR3B |= ((1 << CS31)| (1 << CS30));        //define prescaler de 64 para timer 3 e inicia
  //--------------------------------------------TIMER 4:
  //PWM
  //Ativa pinos do canal 3:
  DDRH |= (1 << PH3);                //ativa pino PH3 (OC4A) - pino 6
  DDRH |= (1 << PH4);                //ativa pino PH4 (OC4B) - pino 7
  DDRH |= (1 << PH5);                //ativa pino PH5 (OC4C) - pino 8
  //Limpa registradores de controle do timer 4
  TCCR4A = 0;
  TCCR4B = 0;
  //Define timer 4 no modo Fast-PWM 14 (TOP definido por ICR4)
  TCCR4A |= (1 << WGM41);
  TCCR4B |= (1 << WGM42);
  TCCR4B |= (1 << WGM43);
  //Saida de PWM nao-inversor em A e inversor em B e nao-inversor em C
  TCCR4A |= ((1 << COM4A1) | (1 << COM4B1) | (1 << COM4B0) | (1 << COM4C1));
  TCCR4A &= ~(1 << COM4A0);
  TCCR4A &= ~(1 << COM4C0);
  // Define frequencia do PWM e Duty Cycle
  ICR4 = ((IPI*1000 + H)/4-1);
  OCR4A = ICR4/(((1000*IPI)+H)/H);
  OCR4B = ICR4/(((1000*IPI)+H)/H);
  OCR4C = ICR4/(((1000*IPI)+H)/H);
  //Inicia timer 4
  TCCR4B |= ((1 << CS41)| (1 << CS40));        //define prescaler de 64 para timer 4 e inicia
  //--------------------------------------------TIMER 5:
  //PWM
  //Ativa pinos do canal 4:
  DDRL |= (1 << PL3);                //ativa pino PL3 (OC5A) - pino 46
  DDRL |= (1 << PL4);                //ativa pino PL4 (OC5B) - pino 45
  DDRL |= (1 << PL5);                //ativa pino PL5 (OC5C) - pino 44
  //Limpa registradores de controle do timer 5
  TCCR5A = 0;
  TCCR5B = 0;
  //Define timer 5 no modo Fast-PWM 14 (TOP definido por ICR5)
  TCCR5A |= (1 << WGM51);
  TCCR5B |= (1 << WGM52);
  TCCR5B |= (1 << WGM53);
  //Saida de PWM nao-inversor em A e inversor em B e nao-inversor em C
  TCCR5A |= ((1 << COM5A1) | (1 << COM5B1) | (1 << COM5B0) | (1 << COM5C1));
  TCCR5A &= ~(1 << COM5A0);
  TCCR5A &= ~(1 << COM5C0);
  // Define frequencia do PWM e Duty Cycle
  ICR5 = ((IPI*1000 + H)/4-1);
  OCR5A = ICR5/(((1000*IPI)+H)/H);
  OCR5B = ICR5/(((1000*IPI)+H)/H);
  OCR5C = ICR5/(((1000*IPI)+H)/H);
  //Inicia timer 5
  TCCR5B |= ((1 << CS51)| (1 << CS50));        //define prescaler de 64 para timer 5 e inicia  
  sei();                                       //Reativa os interrupts
}

void config_tbs_unipolar()
{
  cli();                                      //desativa interrupts para fazer configuracao
  //--------------------------------------------TIMER 1:
  //PWM
  //Ativa pinos do canal 1:
  DDRB |= (1 << PB5);                //ativa pino PB5 (OC1A) - pino 11
  DDRB |= (0 << PB6);                //desativa pino PB6 (OC1B) - pino 12
  DDRB |= (1 << PB7);                //ativa pino PB7 (OC1C) - pino 13
  //Limpa registradores de controle do timer 1
  TCCR1A = 0;
  TCCR1B = 0;
  //Define timer 1 no modo Fast-PWM 14 (TOP definido por ICR1)
  TCCR1A |= (1 << WGM11);
  TCCR1B |= (1 << WGM12);
  TCCR1B |= (1 << WGM13);
  //Saida de PWM nao-inversor em A e inversor em B e nao-inversor em C
  TCCR1A |= ((1 << COM1A1) | (1 << COM1B1) | (1 << COM1B0) | (1 << COM1C1));
  TCCR1A &= ~(1 << COM1A0);
  TCCR1A &= ~(1 << COM1C0);
  //Define frequencia do PWM e Duty Cycle
  ICR1 = (250*(IPI+H))-1;
  OCR1A = ICR1/((IPI+H)/H);
  OCR1B = ICR1/((IPI+H)/H);
  OCR1C = ICR1/((IPI+H)/H);
  //Inicia timer 1
  TCCR1B |= ((1 << CS11)| (1 << CS10));        //define prescaler de 64 para timer 1 e incia
  //Ativa interrupt do timer 1:
  TIMSK1 |= (1<<OCIE1A);
  //--------------------------------------------TIMER 3:
  //PWM
  //Ativa pinos do canal 2:
  DDRE |= (1 << PE3);                //ativa pino PE3 (OC3A) - pino 5
  DDRE |= (0 << PE4);                //desativa pino PE4 (OC3B) - pino 2
  DDRE |= (1 << PE5);                //ativa pino PE5 (OC3C) - pino 3
  //Limpa registradores de controle do timer 3
  TCCR3A = 0;
  TCCR3B = 0;
  //Define timer 3 no modo Fast-PWM 14 (TOP definido por ICR3)
  TCCR3A |= (1 << WGM31);
  TCCR3B |= (1 << WGM32);
  TCCR3B |= (1 << WGM33);
  //Saida de PWM nao-inversor em A e inversor em B e nao-inversor em C
  TCCR3A |= ((1 << COM3A1) | (1 << COM3B1) | (1 << COM3B0) | (1 << COM3C1));
  TCCR3A &= ~(1 << COM3A0);
  TCCR3A &= ~(1 << COM3C0);
  // Define frequencia do PWM e Duty Cycle
  ICR3 = (250*(IPI+H))-1;
  OCR3A = ICR3/((IPI+H)/H);
  OCR3B = ICR3/((IPI+H)/H);
  OCR3C = ICR3/((IPI+H)/H);
  //Inicia timer 3
  TCCR3B |= ((1 << CS31)| (1 << CS30));        //define prescaler de 64 para timer 3 e inicia 
  //--------------------------------------------TIMER 4:
  //PWM
  //Ativa pinos do canal 3:
  DDRH |= (1 << PH3);                //ativa pino PH3 (OC4A) - pino 6
  DDRH |= (0 << PH4);                //desativa pino PH4 (OC4B) - pino 7
  DDRH |= (1 << PH5);                //ativa pino PH5 (OC4C) - pino 8
  //Limpa registradores de controle do timer 4
  TCCR4A = 0;
  TCCR4B = 0;
  //Define timer 4 no modo Fast-PWM 14 (TOP definido por ICR4)
  TCCR4A |= (1 << WGM41);
  TCCR4B |= (1 << WGM42);
  TCCR4B |= (1 << WGM43);
  //Saida de PWM nao-inversor em A e inversor em B e nao-inversor em C
  TCCR4A |= ((1 << COM4A1) | (1 << COM4B1) | (1 << COM4B0) | (1 << COM4C1));
  TCCR4A &= ~(1 << COM4A0);
  TCCR4A &= ~(1 << COM4C0);
  //Define frequencia do PWM e Duty Cycle
  ICR4 = (250*(IPI+H))-1;
  OCR4A = ICR4/((IPI+H)/H);
  OCR4B = ICR4/((IPI+H)/H);
  OCR4C = ICR4/((IPI+H)/H);
  //Inicia timer 4
  TCCR4B |= ((1 << CS41)| (1 << CS40));        //define prescaler de 64 para timer 4 e inicia
  //--------------------------------------------TIMER 5:
  //PWM
  //Ativa pinos do canal 4:
  DDRL |= (1 << PL3);                //ativa pino PL3 (OC5A) - pino 46
  DDRL |= (0 << PL4);                //desativa pino PL4 (OC5B) - pino 45
  DDRL |= (1 << PL5);                //ativa pino PL5 (OC5C) - pino 44
  //Limpa registradores de controle do timer 5
  TCCR5A = 0;
  TCCR5B = 0;
  //Define timer 5 no modo Fast-PWM 14 (TOP definido por ICR5)
  TCCR5A |= (1 << WGM51);
  TCCR5B |= (1 << WGM52);
  TCCR5B |= (1 << WGM53);
  //Saida de PWM nao-inversor em A e inversor em B e nao-inversor em C
  TCCR5A |= ((1 << COM5A1) | (1 << COM5B1) | (1 << COM5B0) | (1 << COM5C1));
  TCCR5A &= ~(1 << COM5A0);
  TCCR5A &= ~(1 << COM5C0);
  // Define frequencia do PWM e Duty Cycle
  ICR5 = (250*(IPI+H))-1;
  OCR5A = ICR5/((IPI+H)/H);
  OCR5B = ICR5/((IPI+H)/H);
  OCR5C = ICR5/((IPI+H)/H);
  //Inicia timer 5
  TCCR5B |= ((1 << CS51)| (1 << CS50));        //define prescaler de 64 para timer 5 e inicia
  sei();                                       //Reativa os interrupts  
}

//Função principal:
int main()
{  
  char data[30];
  pinMode( 22 , OUTPUT );                       //define saída 22 do Arduino para indicação com LED de protocolos ligados/desligados
  digitalWrite( 22, LOW);
  Serial.begin(9600);                           //inicia comunicação serial 
  sei();                                        //reativa interrupts
  while(1)  
  {
    while(Serial.available()<30);    
    char i;
    for(i=0; i<Serial.available(); i++)
    {
      data[i]=Serial.read();                 //lê dados recebidos na serial       
    }     
    if(data[0] == 'p')
    {
      digitalWrite( 22 , HIGH );  
      if(data[1] == 'b')
      {
        protocol = 'b';
        i=3;     
        int j=0;          
        char Tr[5];               
        while(isDigit(data[i]))
        {
          Tr[j]=data[i];
          i++;
          j++;
        } 
        int comp = j;
        j=0;
        H=0;
        while(j<=comp)
        {
          H += (Tr[j]-'0')*pow(10,(comp-j-1));
          j++;
        }
        i++;
        if(isDigit(data[i+1]))
        {
          IPI = 10*(data[i]-'0')+(data[i+1]-'0');  
          i += 2;          
        }
        else
        {
          IPI =(data[i]-'0');
          i++;
        }
        i++;
        if(isDigit(data[i+1]))
        {
          ITI = 10*(data[i]-'0')+(data[i+1]-'0');                    
        }
        else
        {
          ITI =(data[i]-'0');
          i++;
        }                                                                     
        config_basal_unipolar();  
        
       } 
      if(data[1] == 't')
      {
        protocol = 't';       
        config_tbs_unipolar(); 
      }    
                                                      
     }   
      else if(data[0] == 'd')
      {
        if(data[1] == 'd')
        {
          digitalWrite( 22, LOW); 
          config_desativado();  
        }            
      }
  }
  return 0;
}
